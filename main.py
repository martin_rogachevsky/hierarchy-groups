import argparse
import numpy as np
from classifier import Classifier
from plotBuilder import drawResults

def main():
    args = parseArgs()

    if args.count is not None:
        matrix = np.random.rand(args.count, args.count)
        matrix = (matrix + matrix.T)/2
        print(matrix)
        drawResults(Classifier(matrix).hierarchy)
    

def parseArgs():
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--count', dest='count', type=int)
    return argparser.parse_args()
    

main()
