from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram

def drawResults(linkage_matrix):
    dendrogram(linkage_matrix)
    plt.show()
