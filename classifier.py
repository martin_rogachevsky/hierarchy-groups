from itertools import combinations, product

class Classifier:
    def __init__(self, distanceMatrix):
        self.distanceMatrix = distanceMatrix.copy()
        self.hierarchy = []
        self.classify()

    def classify(self):
        count = len(self.distanceMatrix)
        forest = Classifier.getInitForest(count)
        for step in range(count - 1):
            combiningTrees, newDistance = min((((a, b), self.minDistance(a[1], b[1])) for a, b in Classifier.pairs(forest)), key=lambda tree: tree[1])
            newTree = combiningTrees[0][1] + combiningTrees[1][1]
            forest = self.uncombinedForest(forest, combiningTrees)
            forest.append((count + step, newTree))
            self.hierarchy.append([combiningTrees[0][0], combiningTrees[1][0], newDistance, 2])

    def minDistance(self, tree1, tree2):
        distanses = []
        for pair in Classifier.forestProduct(tree1,tree2):
            distanses.append(self.distanceMatrix[pair[0]][pair[1]])
        return min(distanses)

    def uncombinedForest(self, forest, treesToCombine):
        result = []
        for tree in forest:
            if tree not in treesToCombine:
                result.append(tree)
        return result

    @staticmethod
    def getInitForest(count):
        result = []
        for i in range(count):
            result.append((i, [i]))
        return result

    @staticmethod
    def pairs(iterable):
        return combinations(iterable,2)

    @staticmethod
    def forestProduct(tree1, tree2):
        result = []
        for pair in product(*[tree1, tree2]):
            result.append(pair)
        return result